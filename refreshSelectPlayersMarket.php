<?php
include 'conexionDB.php';


$data ='';

$data ='<option value="0" selected="selected">Elige jugador</option>';

$sql = "SELECT j.id, j.name as nameJugador,j.position, e.name as nameEquipo FROM players j, teams e where j.idTeam=e.id and j.id not in (SELECT id_player from human_players_team) and j.id not in (SELECT id_player from market) order by j.idTeam asc, j.name asc";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $idPlayer = $row['id'];
    $name = $row['nameJugador'];
    $nameEquipo =$row['nameEquipo'];
    $data .='<option value="'.$idPlayer.'">'.$name.' - '.$nameEquipo.'</option>';
}

echo $data;