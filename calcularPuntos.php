<?php
//lanzamos el script al terminar la jornada
include 'conexionDB.php';

$ultimaJornadaComunio = $soap->getlatestgameday();
$ultimaJornada = 0;
$sql = "SELECT DISTINCT jornada FROM players_historico order by jornada desc LIMIT 1";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $ultimaJornada = $row['jornada'];
}

if($ultimaJornada < $ultimaJornadaComunio){


    if($ultimaJornadaComunio == 0){
        header('Location: administracion.php?error=14');
        exit();
    }else{

        $sql = "SELECT * FROM players_historico WHERE jornada = $ultimaJornadaComunio limit 1";
        if (!$resultado = $mysqli->query($sql)) {
            header('Location: administracion.php?error=2');
            exit();
        }

        if ($resultado->num_rows === 0) {
            //Borramo backup
            if (!$mysqli->query("TRUNCATE TABLE players_backup") === TRUE ){
                header('Location: administracion.php?error=12');
                exit();
            }
            //Guardamos backup
            $sql = "SELECT * FROM players";
            if (!$resultado = $mysqli->query($sql)) {
                header('Location: administracion.php?error=3');
                exit();
            }

            while($row = $resultado->fetch_assoc()) {
                $id = $row['id'];
                $idTeam = $row['idTeam'];
                $name = $row['name'];
                $points = $row['points'];
                $valor = $row['value'];
                $position = $row['position'];
                $partidos_jugados = $row['partidos_jugados'];
                $status = $row['status'];
                $racha = $row['racha'];

                $query = "INSERT INTO players_backup VALUES ($id, $idTeam, '$name','$valor','$points','$position','$partidos_jugados','$status','$racha')";
                if (!$resultado2 = $mysqli->query($query)) {
                    header('Location: administracion.php?error=4');
                    exit();
                }

            }
            //Metemos en la tabla del once ideal, comprobamos que no existe ya la jornada
            $sql = "SELECT * FROM once_ideal WHERE jornada = $ultimaJornadaComunio";
            if (!$resultado = $mysqli->query($sql)) {
                header('Location: administracion.php?error=2');
                exit();
            }

            if ($resultado->num_rows === 0) {

                $players = $soap->getbestlineupbygameday($ultimaJornadaComunio);

                foreach($players as $jugador){
                    $id = $jugador->id;
                    $points = $jugador->points;
                    $valor = $jugador->quote;

                    //añadimos el once ideal de esta jornada
                    $query = "INSERT INTO once_ideal VALUES ($id, $ultimaJornadaComunio, $points,0)";
                    if (!$resultado3 = $mysqli->query($query)) {
                        header('Location: administracion.php?error=13');
                        exit();
                    }
                }

            }else{
                header('Location: administracion.php?error=1');
                exit();
            }

            $teams = $soap->getclubs();

            foreach($teams as $equipo){
                $idTeam = $equipo->id;
                $players = $soap->getplayersbyclubid($idTeam);
                foreach($players as $jugador){
                    $id = $jugador->id;
                    $name = utf8_decode(addslashes($jugador->name));
                    $points = $jugador->points;
                    $valor = $jugador->quote;
                    $position = $jugador->position;
                    $partidosJugados = $jugador->rankedgamesnumber;
                    $status = $jugador->status;

                    $racha = 0;
                    //Cogemos el valor de los puntos en nuestra base de datos
                    $sql = "SELECT name, points FROM players WHERE id = $id";
                    if (!$resultado = $mysqli->query($sql)) {
                        header('Location: administracion.php?error=2');
                        exit();
                    }

                    //calculamos los puntos de esta jornada
                    $playersStats= $resultado->fetch_assoc();
                    $puntosJornada = $points - $playersStats['points'];

                    $totalPoints = 0;
                    for ($i = $ultimaJornadaComunio; $i > $ultimaJornadaComunio - 5; $i--) {
                        $sqlRacha = "SELECT points FROM players_historico where id=$id and jornada=$i";
                        $resultadoRacha = $mysqli->query($sqlRacha);

                        while ($rowRacha = $resultadoRacha->fetch_assoc()) {
                            $totalPoints = $totalPoints + $rowRacha['points'];
                        }
                    }

                    $racha = $totalPoints / 5;

                    //añadimos la nueva jornada con los puntos
                   /* $query = "INSERT INTO players_jornada VALUES ($id, $ultimaJornadaComunio, $puntosJornada)";
                    if (!$resultado3 = $mysqli->query($query)) {
                        header('Location: administracion.php?error=5');
                        exit();
                    }*/

                    //Actualizamos en la tabla principal el valor y los puntos
                    $query = "UPDATE players SET points='$points',value='$valor',position='$position', partidos_jugados='$partidosJugados', racha='$racha' where id=$id";
                    if (!$resultado4 = $mysqli->query($query)) {
                        header('Location: administracion.php?error=6');
                        exit();
                    }

                    $query = "INSERT INTO players_historico VALUES ($id, '$valor','$points', '$position','$partidosJugados','$racha','$ultimaJornadaComunio','$status','$idTeam')";
                    if (!$resultado4 = $mysqli->query($query)) {
                        header('Location: administracion.php?error=15');
                        exit();
                    }

                }
            }

            header('Location: administracion.php?success=2');
            exit();

        }else{
            header('Location: administracion.php?error=1');
            exit();
        }

    }

}else{
    $teams = $soap->getclubs();

    foreach($teams as $equipo){
        $idTeam = $equipo->id;
        $players = $soap->getplayersbyclubidinclretired($idTeam);
        foreach($players as $jugador){
            $id = $jugador->id;
            $name = $jugador->name;
            $points = $jugador->points;
            $valor = $jugador->quote;
            $position = $jugador->position;
            $status = $jugador->status;

            //Coger partidos jugados
            $partidosJugados = '';
            $playerPartidosJugados = $soap->getplayersbyclubid($idTeam);
            foreach($playerPartidosJugados as $jugador2){
                if($jugador2->id == $id){
                    $partidosJugados = $jugador2->rankedgamesnumber;
                }
            }

            if(isset($partidosJugados)){


                $totalPoints = 0;
                $racha = 0;
                for ($i = $ultimaJornadaComunio; $i > $ultimaJornadaComunio - 5; $i--) {
                    $sqlRacha = "SELECT points FROM players_historico where id=$id and jornada=$i";
                    $resultadoRacha = $mysqli->query($sqlRacha);

                    while ($rowRacha = $resultadoRacha->fetch_assoc()) {
                        $totalPoints = $totalPoints + $rowRacha['points'];
                    }
                }

                $racha = $totalPoints / 5;

                $query = "UPDATE players SET points='$points',value='$valor',position='$position', partidos_jugados='$partidosJugados',status='$status', racha='$racha' where id=$id";
                $mysqli->query($query);
            }

        }
    }

    header('Location: administracion.php?success=4');
    exit();
}

