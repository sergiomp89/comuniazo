<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">TFG Sergio Benito Robles </a>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="equipos.php"><i class="fa fa-table fa-fw"></i> Equipos</a>
                        </li>
                        <li>
                            <a href="jugadores.php"><i class="fa fa-edit fa-fw"></i> Jugadores</a>
                        </li>
                        <li>
                            <a href="seleccion.php"><i <i class="fa fa-hand-o-right" aria-hidden="true"></i></i> Construir equipo</a>
                        </li>
                        <li>
                            <a href="jugadoresHumanos.php"><i class="fa fa-wrench fa-fw"></i> Jugadores humanos</a>
                        </li>
                        <li>
                            <a href="mejoraEquipo.php"><i <i class="fa fa-bomb" aria-hidden="true"></i></i> Mejorar equipo</a>
                        </li>

                        <li>
                            <a href="market.php"><i <i class="fa fa-bomb" aria-hidden="true"></i></i> Mercado</a>
                        </li>
                        <li>
                            <a href="administracion.php"><i class="fa fa-wrench fa-fw"></i> Administración</a>
                        </li>
                      
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>