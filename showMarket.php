<?php
include 'conexionDB.php';
include 'lib.php';

$data ='';
$sql = "SELECT j.id, j.name as nameJugador,j.position, j.status, j.value as valueJugador, j.points as pointsJugador, e.name as nameEquipo, j.partidos_jugados, j.racha FROM players j, teams e, market m where j.idTeam=e.id and j.id=m.id_player";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $idPlayer = $row['id'];
    $nameJugador = $row['nameJugador'];
    $position = getPosition($row['position']);
    $pointsJugador = number_format($row['pointsJugador'],0,".",".");
    $valueJugador = number_format($row['valueJugador'],0,".",".");

    $media = 0;
    if( $row['partidos_jugados'] > 0)
        $media = number_format($row['pointsJugador'] / $row['partidos_jugados'] ,2,".",".");

    $racha = $row['racha'];

    $data .=' <tr>';
    $data .='<td>'.$nameJugador.'</td>';
    $data .='<td>'.$position.'</td>';
    $data .='<td>'.$pointsJugador.'</td>';
    $data .='<td>'.$valueJugador.' €</td>';
    $data .='<td>'.$media.'</td>';
    $data .='<td>'.$racha.'</td>';
    $data .=' </tr>';
}
echo $data;


