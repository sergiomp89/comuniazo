<?
include 'header.php';
?> 
<body>
 <div id="wrapper">
  <?
  include 'menu.php';
  ?> 
        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Equipos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- /.panel -->
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <input type="text" id="search" style="width:200px"  class="form-control"/>
                                        <br>
                                        <table class="table table-bordered table-hover table-striped" id="tableEquipos">
                                            <thead>
                                                <tr>

                                                    <th>Equipo</th>
                                                    <th>Puntos Totales</th>
                                                    <th>Valor</th>
                                                    <th>Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?
                                            		$sql = "SELECT e.id,e.name as nombreEquipo, SUM(j.points) as points, SUM(j.value) as value FROM teams e, players j WHERE j.idTeam=e.id GROUP BY idTeam order by e.name asc";
                                            		$resultado = $mysqli->query($sql);
                                            		while($row = $resultado->fetch_assoc()) {
                                            			?>
                                            			<tr>

		                                                    <td><?=$row['nombreEquipo']?></td>
		                                                    <td><?=number_format($row['points'],0,".",".")?></td>
		                                                    <td><?=number_format($row['value'],0,".",".")?> €</td>
                                                            <td><a class="btn btn-primary" href="verEquipo.php?id=<?=$row['id']?>" role="button">Ver equipo</a></td>
		                                                </tr>
		                                                <?
                                            		}
                                            	?>  
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

           	</div>     
        </div>
        <!-- /#page-wrapper -->

    </div>

</body>
  <?
include 'footer.php';

?>
<script>
    $.tablesorter.addParser({
        // set a unique id
        id: 'puntos',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('.','');
            s=s.replace(',','.');
            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });

    $.tablesorter.addParser({
        // set a unique id
        id: 'valor',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('€','');
            s=s.replace(new RegExp(/[.]/g), "");
            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });

    $("#search").keyup(function(){
        _this = this;
        // Muestra los tr que concuerdan con la busqueda, y oculta los demás.
        $.each($("#tableEquipos tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                $(this).hide();
            else
                $(this).show();
        });
    });

    $(function() {
        $("#tableEquipos").tablesorter({
            headers: {
                1: {//zero-based column index
                    sorter:'puntos'
                },
                2: {
                    sorter:'valor'
                }
            }
        });
    });
</script>
