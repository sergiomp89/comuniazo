<?php
include 'conexionDB.php';

include 'lib.php';

//Get ultima jornada
$ultimaJornada = 0;
$sql = "SELECT DISTINCT jornada FROM players_historico order by jornada desc LIMIT 1";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $ultimaJornada = $row['jornada'];
}

//Minimo partidos jugados
if($ultimaJornada > 0){
    $minimoPartidos =  ($ultimaJornada * 10) / 100;
}else{
    $minimoPartidos = 1;
}

$criterioValue = $_POST['criterioValue'];
$numero = $_POST['numero'];

$dinero = str_replace(".", "", $_POST['dinero']) ;
$criterio = $_POST['criterio'];

$valueTeam = str_replace(".", "", $_POST['valueTeam']) ;
$totalPresupuesto = $dinero - $valueTeam;

$pointsTeam = $_POST['pointsTeam'];
$mediaTeam = $_POST['mediaTeam'];
$rachaTeam = $_POST['rachaTeam'];

if($criterio == 'media'){
    $criteroSelect = '(points/partidos_jugados) criterio';
}elseif($criterio == 'racha'){
    $criteroSelect = 'racha as criterio';
}elseif($criterio == 'puntos'){
    $criteroSelect = 'points as criterio';
}

$arrayOldTeam = array();
$arrayNewTeam = array();
$arrayPlayers = array();
$sql = "SELECT idPlayer FROM team_compare";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $idP = $row['idPlayer'];
    $sql2 = "SELECT * FROM players where id=$idP";
    $resultado2 = $mysqli->query($sql2);

    //Guardamos datos de antiguo equipo
    while($row2 = $resultado2->fetch_assoc()) {
        $positionPlayer = $row2['position'];
        if($row2['partidos_jugados'] > 0) {
            $row2['media'] = $row2['points'] / $row2['partidos_jugados'];
        }else{
            $row2['media']  = 0;
        }
        $row2['criterio'] = 0;
        if($criterio == 'media'){
            if($row2['partidos_jugados'] > 0) {
                $row2['criterio'] = $row2['points'] / $row2['partidos_jugados'];
            }
        }elseif($criterio == 'racha'){
            $row2['criterio'] = $row2['racha'];
        }elseif($criterio == 'puntos'){
            $row2['criterio'] = $row2['points'];
        }
        $arrayOldTeam[] = $row2;
    }

    //Guardamos los jugadores ordenados por criterio
    $sqlBest = "SELECT id, value, $criteroSelect, name, points,partidos_jugados,position, racha FROM players where position='$positionPlayer' and status='ACTIVE' and partidos_jugados>='$minimoPartidos' order by criterio desc";
    $resultadoBest = $mysqli->query($sqlBest);
    while($row2 = $resultadoBest->fetch_assoc()) {
        if($row2['partidos_jugados'] > 0){
            $row2['media'] = $row2['points'] / $row2['partidos_jugados'];
        }else{
            $row2['media'] = 0;
        }

        $arrayPlayers[$positionPlayer][] = $row2;
    }
}

foreach($arrayPlayers as $key => $player){
    $p = shuffle_assoc($player);
    $arrayPlayers[$key] = $p;
}


$arrayOldTeam = shuffle_assoc($arrayOldTeam);
$k = 2;
define('START_TIME', time());
do{
    $copyArrayOldTeam = $arrayOldTeam;
    $copyArrayPlayers = $arrayPlayers;

    $randomPlayers = array_rand($arrayOldTeam, $k);
    $f = is_array($randomPlayers);
    if($f == false){
        $randomPlayers = array($randomPlayers);
    }

    $totalPresupuesto = $dinero - $valueTeam;


    foreach($randomPlayers as  $playerRandomKey){
        $playerRandom = $arrayOldTeam[$playerRandomKey];
        $positionPlayerRandom = $playerRandom['position'];
        //Eliminamos el id del random old de la lista de players
        foreach($copyArrayPlayers[$playerRandom['position']] as $key => $playerAll){
            if($playerAll['id'] == $playerRandom['id'])
                unset($copyArrayPlayers[$key]);
        }
        $arrayByPos = $copyArrayPlayers[$playerRandom['position']];

        do{
            $randomPlayersByPositionKey = array_rand($arrayByPos, 1);
            $randomPlayerByPositionTemp = $arrayByPos[$randomPlayersByPositionKey];
            //Comprobamos que no existe en el old
            $existeOldTeam = 0;
            foreach($copyArrayOldTeam as $key => $playerAll){
                if($playerAll['id'] == $randomPlayerByPositionTemp['id'])
                    $existeOldTeam = 1;
            }

        }while($existeOldTeam == 1);

        $randomPlayerByPosition = $arrayByPos[$randomPlayersByPositionKey];

        $totalPresupuesto = $totalPresupuesto - $randomPlayerByPosition['value'];

        $totalPresupuesto = $totalPresupuesto + $copyArrayOldTeam[$playerRandomKey]['value'];


        unset($copyArrayOldTeam[$playerRandomKey]);
        $copyArrayOldTeam[$playerRandomKey] = $randomPlayerByPosition;
    }

    $arrayOldTeam2 = shuffle_assoc($copyArrayOldTeam);

    do {
        $hayCambios = count($arrayOldTeam2);

        foreach($arrayOldTeam2 as $key => $player){
            $valueP = $player['value'];
            $criterioP = $player['criterio'];
            $position = $player['position'];

            //Cogemos solo el array de la posicion que nos interesa
            $arrayByPos = $arrayPlayers[$position];

            //Empezamos la comparacion con nuestros jugadores
            foreach($arrayByPos as $playerAll){
                $existePlayer = 0;
                $existePlayer2 = 0;
                if($player['id'] != $playerAll['id']){
                    foreach($arrayOldTeam2 as $player1Check){
                        if($player1Check['id'] == $playerAll['id'])
                            $existePlayer2 = 1;
                    }

                    if(($playerAll['criterio'] > $criterioP) and ((($totalPresupuesto + $valueP) - $playerAll['value']) >= 0) and $existePlayer2 == 0){
                        $arrayOldTeam2[$key] = $playerAll;
                        $totalPresupuesto = ($totalPresupuesto + $valueP) - $playerAll['value'];
                        $existePlayer = 1;
                        break;
                    }
                }
            }
            if($existePlayer == 0)
                $hayCambios --;

        }
    }while($hayCambios > 0);

    $mediaTotal = 0;
    $puntosTotal = 0;
    $valueTotal = 0;
    $rachaTotal = 0;
    $arrayOldTeam2 = orderByPosition($arrayOldTeam2,1);
    if(count($arrayOldTeam2)>0){
        foreach ($arrayOldTeam2 as $equipazo){
            foreach ($equipazo as $player){
                $mediaTotal = $mediaTotal + $player['media'];
                $puntosTotal = $puntosTotal + $player['points'];
                $valueTotal = $valueTotal + $player['value'];
                if($criterio == 'racha') {
                    $rachaTotal = $rachaTotal + $player['racha'];
                }
            }
        }
    }
    $criterioValueNew = '';
    if($criterio == 'racha') {
        $criterioValueNew = $rachaTotal;
    }
    if($criterio == 'media'){
        $criterioValueNew = $mediaTotal;
    }
    if($criterio == 'puntos'){
        $criterioValueNew = $puntosTotal;
    }

    if($criterioValue <= $criterioValueNew){
        break;
    }
    $k++;



}while($k <= $numero);


$data = '';

if($k-1 == $numero){
    $data = 'No hemos podido encontrar un equipo';
    echo $data;
}else {
    $arrayOldTeam3 = orderByPosition($arrayOldTeam2, 0);

    if (count($arrayOldTeam3) > 0) {
        $mediaTotal = 0;
        $puntosTotal = 0;
        $valueTotal = 0;
        $rachaTotal = 0;

        $data .= "<h4>Solución 4</h4>";
        foreach ($arrayOldTeam3 as $equipazo) {
            foreach ($equipazo as $player) {
                $data .= $player['name'];
                $data .= "<br>";
                $mediaTotal = $mediaTotal + $player['media'];
                $puntosTotal = $puntosTotal + $player['points'];
                $valueTotal = $valueTotal + $player['value'];
                if ($criterio == 'racha') {
                    $rachaTotal = $rachaTotal + $player['racha'];
                }
            }
        }

        $data .= "Puntos: " . $puntosTotal;
        $data .= "<br>";
        $data .= "Media: " . number_format($mediaTotal, 3, ',', ' ');
        $data .= "<br>";

        if ($criterio == 'racha') {
            $data .= "Racha: " . $rachaTotal;
            $data .= "<br>";
        }
        $data .= "Valor: " . number_format($valueTotal, 0, ',', '.') . '€';
        $data .= "<br>";

        echo $data;
    }

}