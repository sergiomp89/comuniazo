<?
include 'conexionDB.php';

$numeroJornada = $_POST['jornada']; 

//Cogemos el valor de los puntos en nuestra base de datos
$sql = "SELECT jornada FROM players_historico ORDER BY jornada DESC LIMIT 1";
if (!$resultado = $mysqli->query($sql)) {
   header('Location: administracion.php?error=2');
	exit();
}

$checkJornada= $resultado->fetch_assoc();

if($numeroJornada != $checkJornada['jornada']){
	 header('Location: administracion.php?error=10');
	exit();
}else{
	$query = "DELETE FROM players_jornada where jornada=$numeroJornada";
	if (!$resultado2 = $mysqli->query($query)) {
		header('Location: administracion.php?error=11');
		exit();
	}
	$query = "DELETE FROM once_ideal where jornada=$numeroJornada";
	if (!$resultado2 = $mysqli->query($query)) {
		header('Location: administracion.php?error=11');
		exit();
	}

    $query = "DELETE FROM players_historico where jornada=$numeroJornada";
    if (!$resultado2 = $mysqli->query($query)) {
        header('Location: administracion.php?error=16');
        exit();
    }
    if (!$mysqli->query("TRUNCATE TABLE players") === TRUE ){
        header('Location: administracion.php?error=2');
        exit();
    }

    $sql = "SELECT * FROM players_backup";
    if (!$resultado = $mysqli->query($sql)) {
        header('Location: administracion.php?error=8');
        exit();
    }

    while($row = $resultado->fetch_assoc()) {
        $id = $row['id'];
        $idTeam = $row['idTeam'];
        $name = addslashes($row['name']);
        $points = $row['points'];
        $valor = $row['value'];
        $position = $row['position'];
        $partidos_jugados = $row['partidos_jugados'];
        $status = $row['status'];
        $racha = $row['racha'];

        $query = "INSERT INTO players VALUES ($id, $idTeam, '$name','$valor','$points','$position','$partidos_jugados','$status','$racha')";
        if (!$resultado2 = $mysqli->query($query)) {
            header('Location: administracion.php?error=9');
            exit();
        }

    }

	header('Location: administracion.php?success=3');
	exit();
}


    