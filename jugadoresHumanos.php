<?
include 'header.php';
?>
    <body>
    <div id="wrapper">
        <?
        include 'menu.php';
        ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Jugadores humanos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form id="addHumanPlayer">
                            <legend>Añadir jugador humano</legend>
                            <div class="col-sm-3">
                                <label for="veces">Nombre:</label>
                                <input type="text" class="form-control" name="name" id="name" required>

                            </div>

                            <div class="col-sm-3">
                                <input type="button" id="submit" value="Añadir" style="margin-top: 25px;" class="btn btn-primary">
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- /.panel -->
                <div class="panel panel-default">
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="tableEquipos">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Acción</th>
                                        </tr>
                                        </thead>
                                        <tbody id="humanPlayers">
                                        <?
                                        $sql = "SELECT id,name FROM human_players order by name asc";
                                        $resultado = $mysqli->query($sql);
                                        while($row = $resultado->fetch_assoc()) {
                                            ?>
                                            <tr id="jugadorHumano<?=$row['id']?>">
                                                <td><?=$row['name']?></td>
                                                <td><a class="btn btn-primary" href="verJugadorHumano.php?id=<?=$row['id']?>" role="button">Ver jugador</a>
                                                <a class="btn btn-danger" role="button" onclick="deleteHumanPlayer(<?=$row['id']?>)">Eliminar</a></td>
                                            </tr>
                                            <?
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.col-lg-4 (nested) -->
                            <div class="col-lg-8">
                                <div id="morris-bar-chart"></div>
                            </div>
                            <!-- /.col-lg-8 (nested) -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

        </div>

    </div>

    </body>
<?
include 'footer.php';

?>


<script type="text/javascript">

    $( "#submit" ).click(function() {

        var name = $("#name").val();
        var parametros = {
            "name" : name
        };
        $.ajax({
            url: "addHumanPlayer.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){
                $('#humanPlayers').html(data);
                $('#name').val('');
            }
        });
    });


    function deleteHumanPlayer(idHumanPlayer){
        var parametros = {
            "idHumanPlayer": idHumanPlayer
        };
        $.ajax({
            url: "deleteHumanPlayer.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){
                $('#jugadorHumano'+idHumanPlayer).remove();
            }
        });
    }

</script>
