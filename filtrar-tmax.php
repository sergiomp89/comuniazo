<?php
include 'conexionDB.php';

$dinero = str_replace(".", "", $_POST['dinero']) ;
$numero = $_POST['numero'];
$criterio = $_POST['criterio'];
if(isset($_POST['veces']))
    $veces =str_replace(".", "", $_POST['veces']) ;

$maxPorEquipo = $_POST['maxPorEquipo'];
$formacion = $_POST['formacion'];

$jornada = $_POST['jornada'];
if($formacion > 0){
    $explodeFormacion = explode('-',$formacion);
    $alineacion['keeper'] = 1;
    $alineacion['defender'] = $explodeFormacion[0];
    $alineacion['midfielder'] = $explodeFormacion[1];
    $alineacion['striker'] = $explodeFormacion[2];
}


//Get ultima jornada
$ultimaJornada = 0;
$sql = "SELECT DISTINCT jornada FROM players_historico order by jornada desc LIMIT 1";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $ultimaJornada = $row['jornada'];
}

$data = '';
$arrayResultados = array();
$counter = 0;
$mediaResult = 0;
$arrayJugadores = array();
$arrayBestTeam = array();
$best = array();
//SELECCIONAR ALEATORIO DE BBDD
$sql = "SELECT ph.id, p.name, ph.value, ph.points, ph.partidos_jugados, ph.idTeam,ph.position,ph.status,ph.racha FROM players_historico ph, players p where ph.jornada=$jornada and ph.id=p.id"; //seleccionamos 11 aleatorios y comprobamos
$result = $mysqli->query($sql);
$totalMedia = 0;
$totalValor = 0;
$totalPuntos = 0;
while ($row = $result->fetch_assoc()) {
    $arrayJugadores[] = $row;
}


$counterTeam = 0;
define('START_TIME', time());
//for ($i = 1; $i <= $veces; $i++) {
while (time() - START_TIME < $veces){
    shuffle($arrayJugadores);
    $arrayEquipos = array();
    $counter = 0;
    $arrayIdsJugadores = array();
    if($formacion > 0){
        //Keeper
        foreach ($arrayJugadores as $jugador){
            if($jugador['position'] == 'keeper' && $jugador['status'] == 'ACTIVE'){
                $arrayIdsJugadores[] = $jugador['id'];
                $arrayEquipos[$counter][] = $jugador;
                break;
            }

        }
        //Defensas
        for ($d = 1; $d <= $alineacion['defender']; $d++) {
            foreach ($arrayJugadores as $jugador){
                if($jugador['position'] == 'defender' && !in_array($jugador['id'],$arrayIdsJugadores) && $jugador['status'] == 'ACTIVE'){
                    $arrayIdsJugadores[] = $jugador['id'];
                    $arrayEquipos[$counter][] = $jugador;
                    break;
                }

            }
        }
        //Medios
        for ($d = 1; $d <= $alineacion['midfielder']; $d++) {
            foreach ($arrayJugadores as $jugador){
                if($jugador['position'] == 'midfielder' && !in_array($jugador['id'],$arrayIdsJugadores) && $jugador['status'] == 'ACTIVE'){
                    $arrayIdsJugadores[] = $jugador['id'];
                    $arrayEquipos[$counter][] = $jugador;
                    break;
                }

            }
        }
        //Delanteros
        for ($d = 1; $d <= $alineacion['striker']; $d++) {
            foreach ($arrayJugadores as $jugador){
                if($jugador['position'] == 'striker' && !in_array($jugador['id'],$arrayIdsJugadores) && $jugador['status'] == 'ACTIVE'){
                    $arrayIdsJugadores[] = $jugador['id'];
                    $arrayEquipos[$counter][] = $jugador;
                    break;
                }

            }
        }

        if(count($arrayEquipos[$counter]) < $numero){
            $vecesRestantes = $numero - count($arrayEquipos[$counter]);
            for ($d = 1; $d <= $vecesRestantes; $d++) {
                $jugador = array_rand($arrayJugadores);
                $jugador = $arrayJugadores[$jugador];
                if((!in_array($jugador['id'],$arrayIdsJugadores)) && ($jugador['status'] == 'ACTIVE')){
                    $arrayEquipos[$counter][] = $jugador;
                }else{
                    $d--;
                }

            }
        }

    }else{
        for ($d = 1; $d <= $numero; $d++) {
            $jugador = array_rand($arrayJugadores);
            $jugador = $arrayJugadores[$jugador];
            if((!in_array($jugador['id'],$arrayIdsJugadores)) && ($jugador['status'] == 'ACTIVE')){
                $arrayEquipos[$counter][] = $jugador;
            }else{
                $d--;
            }

        }

    }
    $counter++;

    //}



    foreach ($arrayEquipos as  $equipo){
        $team = true;
        $equiposJugadores = array();
        $valueTeam = 0;
        $pointsTeam = 0;
        $totalMedia = 0;
        $racha = 0;
        foreach ($equipo as $jugador){
            $valueTeam = $valueTeam + $jugador['value'];
            $pointsTeam = $pointsTeam + $jugador['points'];
            if ($jugador['partidos_jugados'] > 0) {             //calculamos la media de cada jugador
                $media = $jugador['points'] / $jugador['partidos_jugados'];
            } else {
                $media = 0;
            }
            $totalMedia = $totalMedia + $media;

            $equiposJugadores[] = $jugador['idTeam'];
            $racha = $racha + $jugador['racha'];
        }




        //Check si hay mas jugadores del mismo equipo
        if($maxPorEquipo > 0){
            $team_array = array_count_values($equiposJugadores);
            while (list ($key, $val) = each ($team_array))
            {
                if($val >= $maxPorEquipo)
                    $team = false;
            }
        }

        if(($team) and ($valueTeam <= $dinero)){
            $arrayBestTeam[$counterTeam]['racha'] = $racha;
            $arrayBestTeam[$counterTeam]['equipo'] = $equipo;
            $arrayBestTeam[$counterTeam]['valueTeam'] = number_format($valueTeam, 0, ',', '.');
            $arrayBestTeam[$counterTeam]['pointsTeam'] = $pointsTeam;
            $arrayBestTeam[$counterTeam]['mediaTeam'] = number_format($totalMedia, 3, ',', ' ');

        }
        $counterTeam++;

    }



    if($criterio == 'media'){     // si seleccionamos como criterio la media, recorremos el array de soluciones buscando la que tenga mejor media


        foreach ($arrayBestTeam as $result){

            if(str_replace(",", "", $result['mediaTeam']) > str_replace(",", "", $mediaResult)){
                unset($best);
                $mediaResult = $result['mediaTeam'];
                $best[] = $result;


            }
        }
    }

    if($criterio == 'racha'){
        $rachaResult = 0;
        $best = array();
        foreach ($arrayBestTeam as $result){
            if($result['racha'] > $rachaResult){
                unset($best);
                $rachaResult = $result['racha'];
                $best[] = $result;
            }
        }
    }

    if($criterio == 'puntos'){    // si seleccionamos como criterio los puntos, recorremos el array de soluciones buscando la fila q tenga el mejor puntos
        $puntosResult = 0;
        $best = array();
        foreach ($arrayBestTeam as $result){
            if($result['pointsTeam'] > $puntosResult){
                unset($best);
                $puntosResult = $result['pointsTeam'];
                $best[] = $result;
            }
        }
    }

    if($criterio == 'costePunto'){
        /*$puntosResult = 99999999999999999;
        $best = array();
        foreach ($arrayBestTeam as $result){
            $val = str_replace(".", "", $result['valueTeam']);
            $poi = str_replace(".", "", $result['pointsTeam']);

            $costePorPunto = $val / $poi;
            if($costePorPunto < $puntosResult){
                unset($best);
                $puntosResult = $costePorPunto;
                $best[] = $result;
            }
        }*/
    }

    if($criterio == 'costePuntoMedio'){
        /*$puntosResult = 99999999999999999;
        $best = array();
        foreach ($arrayBestTeam as $result){
            $costePorPuntoMedio = $result['mediaTeam'] / $result['valueTeam'];
            if($costePorPuntoMedio < $puntosResult){
                unset($best);
                $puntosResult = $costePorPuntoMedio;
                $best[] = $result;
            }
        }*/
    }

}



if(count($best)>0){


    $data .= "<div class='row'>";
    $data .= "<div class='col-sm-8'>";
    $data .= "<h4>Solución Tmax</h4>";

    foreach ($best[0]['equipo'] as $equipazo){
        $idP = $equipazo["id"];
        $data .= $equipazo['name'];
        $data .= "<br>";
    }

    $data .= "Puntos: ".$best[0]['pointsTeam'];
    $data .= "<br>";
    $data .= "Media: ".$best[0]['mediaTeam'];
    $data .= "<br>";
    if($criterio == 'racha') {
        $data .= "Racha: " . $best[0]['racha'];
        $data .= "<br>";
    }
    if($criterio == 'costePunto') {
        $val = str_replace(".", "", $result['valueTeam']);
        $poi = str_replace(".", "", $result['pointsTeam']);
        $data .= "Coste por punto: " .  $val / $poi;
        $data .= "<br>";
    }

    if($criterio == 'costePuntoMedio') {
        $data .= "Coste por punto medio: " . $best[0]['mediaTeam'] / $best[0]['valueTeam'];
        $data .= "<br>";
    }
    $data .= "Valor: ".$best[0]['valueTeam'].'€';
    $data .= "<br>";
    $data .= " <form>";
    $p = $best[0]["pointsTeam"];
    $m = $best[0]["mediaTeam"];
    $v = $best[0]["valueTeam"];
    $r = $best[0]["racha"];
    $data .= "<input type='hidden' name='p' id='p' value='$p'>";
    $data .= "<input type='hidden' name='m' id='m' value='$m'>";
    $data .= "<input type='hidden' name='v' id='v' value='$v'>";
    $data .= "<input type='hidden' name='r' id='r' value='$r'>";


    $data .= " </form>";
    $data .="</div>";

}else{
    $data .= "<div class='row'>";
    $data .= "<div class='col-sm-12'>";
    $data .= "No hemos podido encontrar un equipo";
    $data .="</div>";
    $data .="</div>";
}


echo $data;







