<?php
include 'conexionDB.php';
include 'lib.php';

$criterio = $_POST['criterio'];
$saldo = str_replace(".", "", $_POST['saldo']) ;

$formaciones = getArrayFormacion();
$positions = ['keeper','defender','midfielder','striker'];

if(isset($_POST['overheadPorcentajeOption']))
$overheadPorcentajeOption = $_POST['overheadPorcentajeOption'];

if(isset($_POST['overheadCashOption']))
$overheadCashOption = $_POST['overheadCashOption'];

$overheadRadio = $_POST['overheadRadio'];

if($criterio == 'media'){
    $criteroSelect = ',(j.points/j.partidos_jugados) criterio';
}elseif($criterio == 'racha'){
    $criteroSelect = ',j.racha as criterio';
}elseif($criterio == 'puntos'){
    $criteroSelect = ',j.points as criterio';
}

//Get ultima jornada
$ultimaJornada = 0;
$sql = "SELECT DISTINCT jornada FROM players_historico order by jornada desc LIMIT 1";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $ultimaJornada = $row['jornada'];
}

$idHumanPlayer = $_POST['idHumanPlayer'];

$arrayGlobal = array();
$arrayGlobalByPos = array();

$arrayEquipoHumano = array();

$presupuestoTotal = 0;

$sql = "SELECT j.id, j.name as nameJugador,j.position, j.status, j.value as valueJugador, j.points as pointsJugador, e.name as nameEquipo, j.partidos_jugados, j.racha $criteroSelect FROM players j, teams e, human_players_team hpt where j.idTeam=e.id and j.id=hpt.id_player and hpt.id_human_player=$idHumanPlayer order by criterio desc";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $idPlayer = $row['id'];
    $nameJugador = $row['nameJugador'];
    $position = getPosition($row['position']);
    $pointsJugador = number_format($row['pointsJugador'],0,".",".");
    $valueJugador = number_format($row['valueJugador'],0,".",".");
    $presupuestoTotal = $presupuestoTotal + $row['valueJugador'];
    $row['media'] = 0;
    if( $row['partidos_jugados'] > 0)
        $row['media'] = number_format($row['pointsJugador'] / $row['partidos_jugados'] ,2,".",".");

    $row['criterio'] = 0;
    if($criterio == 'media'){
        if($row['partidos_jugados'] > 0) {
            $row['criterio'] = $row['pointsJugador'] / $row['partidos_jugados'];
        }
    }elseif($criterio == 'racha'){
        $row['criterio'] = $row['racha'];
    }elseif($criterio == 'puntos'){
        $row['criterio'] = $row['points'];
    }


    $arrayGlobal[] = $row;
    $arrayGlobalByPos[$row['position']][] = $row;
    $arrayEquipoHumano[$row['position']][] = $row;

}


$arrayEquipoMarket = array();

$sql = "SELECT j.id, j.name as nameJugador,j.position, j.status, j.value as valueJugador, j.points as pointsJugador, e.name as nameEquipo, j.partidos_jugados, j.racha $criteroSelect FROM players j, teams e, market m where j.idTeam=e.id and j.id=m.id_player order by criterio desc";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $idPlayer = $row['id'];
    $nameJugador = $row['nameJugador'];
    $position = getPosition($row['position']);
    $pointsJugador = number_format($row['pointsJugador'],0,".",".");
    $valueJugador = number_format($row['valueJugador'],0,".",".");

    $row['media'] = 0;
    if( $row['partidos_jugados'] > 0)
        $row['media'] = number_format($row['pointsJugador'] / $row['partidos_jugados'] ,2,".",".");

    $row['criterio'] = 0;
    if($criterio == 'media'){
        if($row['partidos_jugados'] > 0) {
            $row['criterio'] = $row['pointsJugador'] / $row['partidos_jugados'];
        }
    }elseif($criterio == 'racha'){
        $row['criterio'] = $row['racha'];
    }elseif($criterio == 'puntos'){
        $row['criterio'] = $row['points'];
    }

    //If overhead is percent
    if($overheadRadio == 'porcentaje'){
        $valueJugadorTemp = ($valueJugador*$overheadPorcentajeOption)/100;
        $row['valueJugador'] = $row['valueJugador'] + $valueJugadorTemp;
    }elseif($overheadRadio == 'cantidad'){
        $row['valueJugador'] = $row['valueJugador'] + $overheadCashOption;
    }

    $arrayGlobal[] = $row;
    $arrayGlobalByPos[$row['position']][] = $row;

    $arrayEquipoMarket[$row['position']][] = $row;

}
//Ordenar por criterio asc
foreach ($arrayGlobal as $key => $row) {
    $aux[$key] = $row['criterio'];
}
array_multisort($aux, SORT_ASC, $arrayGlobal);

$worstTeamByFormacion = array();

foreach($formaciones as $formacion) {
    $explodeFormacion = explode('-', $formacion);
    $alineacion = array();
    $alineacion['keeper'] = 1;
    $alineacion['defender'] = $explodeFormacion[0];
    $alineacion['midfielder'] = $explodeFormacion[1];
    $alineacion['striker'] = $explodeFormacion[2];

    foreach($arrayGlobal as $playerArrayGlobal){
        $positionPlayerArray = $playerArrayGlobal['position'];
        $numByPosition = $alineacion[$positionPlayerArray];
        if(isset($firstArrayTeamByFormacion[$formacion][$positionPlayerArray])){
            if(count($firstArrayTeamByFormacion[$formacion][$positionPlayerArray]) < $numByPosition)
                $firstArrayTeamByFormacion[$formacion][$positionPlayerArray][] = $playerArrayGlobal;
        }else{
            $firstArrayTeamByFormacion[$formacion][$positionPlayerArray][] = $playerArrayGlobal;
        }
    }

}

//Comparamos
$resumenArray = array();

$data = '';
foreach($arrayGlobalByPos as $key => $player){
    $p = shuffle_assoc($player);
    $arrayGlobalByPos[$key] = $p;
}

$arrayNewByPosition = array();

foreach($firstArrayTeamByFormacion as $formacion => $teamByFormacion){

    $arrayCambios = array();

    $presupuestoTotal2 = $presupuestoTotal + $saldo;

    $explodeFormacion = explode('-',$formacion);
    $alineacion['keeper'] = 1;
    $alineacion['defender'] = $explodeFormacion[0];
    $alineacion['midfielder'] = $explodeFormacion[1];
    $alineacion['striker'] = $explodeFormacion[2];

    $arrayTempPlayersFormacion = array();
    foreach($teamByFormacion as $pos => $player){
        foreach($player as $p){
            $arrayTempPlayersFormacion[] = $p;

        }
    }
    //Mezclamos el array de todos los jugadores
    $arrayTempPlayersFormacion = shuffle_assoc($arrayTempPlayersFormacion);


    $alineacionAcabada = 0;

    do{
        $actualCounterPosition = 0;
        //Empezamos la comparacion con nuestros jugadores
        foreach($arrayTempPlayersFormacion as $playerByFormacion){
            $valueP = $playerByFormacion['valueJugador'];
            $criterioP = $playerByFormacion['criterio'];
            $position = $playerByFormacion['position'];

            //Cogemos solo el array de la posicion que nos interesa
            $arrayByPos = $arrayGlobalByPos[$position];
            $counterPosition = $alineacion[$position];
            if(isset($arrayNewByPosition[$formacion][$position])){
                $actualCounterPosition = count($arrayNewByPosition[$formacion][$position]);
            }else{
                $actualCounterPosition = 0;
            }



            //Ordenar por criterio asc
            $aux = array();
            foreach ($arrayByPos as $key => $row) {
                $aux[$key] = $row['criterio'];
            }
            array_multisort($aux, SORT_DESC, $arrayByPos);
            $existePlayer3 = 0;
            //Empezamos la comparacion con nuestros jugadores
            foreach($arrayByPos as $playerAll){
                $existePlayer = 0;
                $existePlayer2 = 0;
                if($playerByFormacion['id'] != $playerAll['id']){
                    if(isset($arrayNewByPosition[$formacion])){
                        if(isset($arrayNewByPosition[$formacion][$position])){
                            foreach($arrayNewByPosition[$formacion][$position] as $player1Check){
                                if($player1Check['id'] == $playerAll['id'])
                                    $existePlayer2 = 1;
                            }
                        }
                    }

                    if(($playerAll['criterio'] > $criterioP) and ($actualCounterPosition < $counterPosition) and (($presupuestoTotal2- $playerAll['valueJugador']) >= 0) and $existePlayer2 == 0){
                        $arrayNewByPosition[$formacion][$position][] = $playerAll;
                        $presupuestoTotal2 = $presupuestoTotal2  - $playerAll['valueJugador'];
                        $existePlayer = 1;
                        break;
                    }
                }else{
                    if(isset($arrayNewByPosition[$formacion])){
                        if(isset($arrayNewByPosition[$formacion][$position])){
                            foreach($arrayNewByPosition[$formacion][$position] as $player1Check){
                                if($player1Check['id'] == $playerAll['id'])
                                    $existePlayer3 = 1;
                            }
                        }
                    }
                }
            }

            if($existePlayer == 0 and $existePlayer3 == 0 and ($actualCounterPosition < $counterPosition)){
                $arrayNewByPosition[$formacion][$position][] = $playerByFormacion;
                $presupuestoTotal2 = $presupuestoTotal2  - $valueP;

            }

        }

        //Comprobamos si tiene la alineacion acabada
        $isFinished = true;
        foreach($alineacion as $position1 => $counter){
            $counterPosition1 = $alineacion[$position1];
            if($arrayNewByPosition[$formacion][$position1]){
                if($counter != count($arrayNewByPosition[$formacion][$position1]))
                    $isFinished = false;
            }else{
                $isFinished = false;
            }

        }


    }while ($isFinished == false);

    $arrayNewByPosition[$formacion][]= $presupuestoTotal2;


}

foreach($arrayNewByPosition as $keyFormacion => $formacionArray) {
    $presupuestoFinal = $formacionArray[0];
    $arrayCompras = array();
    $arrayVentas = array();

    unset($formacionArray[0]);
    $arrayTeam = orderByPosition($formacionArray, 0);
    $data .= "<div class=\"col-lg-2\"><a class=\"btn btn-success\" role=\"button\" onclick=\"showFormacion('formacion$keyFormacion')\">$keyFormacion</a><div id='formacion$keyFormacion' style='display: none'>";
    if($presupuestoFinal > 0){
        foreach ($arrayTeam as $position => $player) {
            $pos = getPosition($position);
            $data .= "<h4>$pos</h4>";
            foreach ($player as  $p) {
                $data .= $p['nameJugador']."<br>";

                //Comprobamos si el jugador estaba en el equipo inicial
                foreach ($arrayEquipoHumano as $positionMine => $playerMine) {
                    if($positionMine == $position){
                        $existeInicio = 0;
                        foreach ($playerMine as  $pMine) {
                            if($pMine['id'] == $p['id'])
                                $existeInicio = 1;
                        }

                        if($existeInicio == 0)
                            $arrayCompras[] = "Hemos comprado a ".$p['nameJugador']." por ".number_format($p['valueJugador'], 0, ',', '.')."€";
                    }

                }
            }

        }

        foreach ($arrayEquipoHumano as $position => $player) {
            foreach ($player as  $p) {

                //Comprobamos si el jugador estaba en el equipo inicial
                foreach ($arrayTeam as $positionMine => $playerMine) {
                    if($positionMine == $position){
                        $existeInicio = 0;
                        foreach ($playerMine as  $pMine) {
                            if($pMine['id'] == $p['id'])
                                $existeInicio = 1;
                        }

                        if($existeInicio == 0)
                            $arrayVentas[] = "Hemos vendido a ".$p['nameJugador']." por ".number_format($p['valueJugador'], 0, ',', '.')."€";
                    }

                }
            }

        }
        $data .= "<br><br>";
        $data .= "<span style='color:orange'>Presupuesto restante: ".number_format($presupuestoFinal, 0, ',', '.').'€</span>';
        $data .= "<br><br><span style='color:green'>Compras</span><br><br>";
        foreach ($arrayCompras as $compras) {
            $data .= "- ".$compras.'<br>';
        }

        $data .= "<br><br><span style='color:red'>Ventas</span><br><br>";
        foreach ($arrayVentas as $ventas) {
            $data .= "- ".$ventas.'<br>';

        }
    }else{
        $data .= "<span style='color:red'>No se puede proporcionar un equipo</span>";

    }
    $data.="</div>";
    $data.="</div>";

}
echo $data;
