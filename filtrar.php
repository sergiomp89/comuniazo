<?php
include 'conexionDB.php';

$dinero = $_POST['dinero'];
$numero = $_POST['numero'];
$criterio = $_POST['criterio'];
if(isset($_POST['veces']))
$veces = $_POST['veces'];

$maxPorEquipo = $_POST['maxPorEquipo'];
$formacion = $_POST['formacion'];


$data = '';
$arrayResultados = array();
$counter = 0;


for ($i = 1; $i <= $veces; $i++) {
    //TODO recorrer las tablas tantas veces como $veces tengamos y guardar en array
    $arrayJugadores = array();
    //SELECCIONAR ALEATORIO DE BBDD
    $sql = "SELECT id, name, value, points,partidos_jugados FROM players ORDER BY rand() LIMIT $numero"; //seleccionamos 11 aleatorios y comprobamos
    $result = $mysqli->query($sql);
    $totalMedia = 0;
    $totalValor = 0;
    $totalPuntos = 0;
    while ($row = $result->fetch_assoc()) {
        $value = $row['value'];
        $name = $row['name'];
        $points = $row['points'];
        $totalValor = $totalValor + $row['value'];
        $totalPuntos = $totalPuntos + $row['points'];
        if ($row['partidos_jugados'] > 0) {             //calculamos la media de cada jugador
            $row['media'] = $row['points'] / $row['partidos_jugados'];
        } else {
            $row['media'] = 0;
        }

        $totalMedia = $totalMedia + $row['media'];    //calculamos media total del equipo
        $arrayJugadores[] = $row;                     //metemos en el array los $numero de jugadores que nos ha devuelto aleatoriamente
    }

    if ($totalValor <= $dinero) {       // si entra dentro del rango guardamos en el array de posibles soluciones

        $arrayResultados[$counter]['players'] = $arrayJugadores;
        $arrayResultados[$counter]['valor'] = $totalValor;
        $arrayResultados[$counter]['mediaEquipo'] = $totalMedia / $numero;
        $arrayResultados[$counter]['totalPuntos'] = $totalPuntos;
        $counter++;
    }

}

if($criterio == 'media'){     // si seleccionamos como criterio la media, recorremos el array de soluciones buscando la que tenga mejor media
    $mediaResult = 0;
    $best = array();
    foreach ($arrayResultados as $result){
        if($result['mediaEquipo'] > $mediaResult){
            unset($best);
            $mediaResult = $result['mediaEquipo'];
            $best[] = $result;
        }
    }
}

if($criterio == 'racha'){

}

if($criterio == 'puntos'){    // si seleccionamos como criterio los puntos, recorremos el array de soluciones buscando la fila q tenga el mejor puntos
    $puntosResult = 0;
    $best = array();
    foreach ($arrayResultados as $result){
        if($result['totalPuntos'] > $puntosResult){
            unset($best);
            $puntosResult = $result['totalPuntos'];
            $best[] = $result;
        }
    }
}

if(count($best)>0){               // si hay solucion, mostramos el mejor equipo sus puntos y su media.
    $data .= "<h2>Mejor Equipo</h2>";
    foreach ($best[0]['players'] as $equipazo){

        $data .= $equipazo['name'];
        $data .= "<br>";
    }

    $data .= "Puntos: ".$best[0]['totalPuntos'];
    $data .= "<br>";
    $data .= "Media: ".$best[0]['mediaEquipo'];
    $data .= "<br>";
}


//TODO Insert max value en tabla