<?php
//lanzamos el script al inicio de la liga y al final de cada periodo de fichaje
include 'conexionDB.php';

//actualizamos equipos

$teams = $soap->getclubs();
//borramos lo anterior
if (!$mysqli->query("TRUNCATE TABLE teams") === TRUE ){
	printf("Fallo al limpiar la tabla de equipos");
}

foreach($teams as $equipo){
	$idTeam = $equipo->id;
	$name = $equipo->name;
	$query = "INSERT INTO teams VALUES ($idTeam, '$name')";
	$mysqli->query($query);
}

//actualizamos jugadores
//borramos lo anterior

if (!$mysqli->query("TRUNCATE TABLE players") === TRUE ){
	printf("Fallo al limpiar la tabla de jugadores");
}

$ultimaJornadaComunio = $soap->getlatestgameday();

foreach($teams as $equipo){
	$idTeam = $equipo->id;
	$players = $soap->getplayersbyclubidinclretired($idTeam);//Necesitamos utilizar el retired, por si se marcha algun jugador a mitad de temporada
	foreach($players as $jugador){
		$id = $jugador->id;
		$name = $jugador->name;//utf8 y addslashes para tildes y jugadores como N'zonzi
		$points = $jugador->points;
		$valor = $jugador->quote;
		$position = $jugador->position;
		$status = $jugador->status;

		//Coger partidos jugados
		$partidosJugados = 0;
		$playerPartidosJugados = $soap->getplayersbyclubid($idTeam);
		foreach($playerPartidosJugados as $jugador2){
			if($jugador2->id == $id){
				$partidosJugados = $jugador2->rankedgamesnumber;
			}
		}

        $totalPoints = 0;
        $racha = 0;
        for ($i = $ultimaJornadaComunio; $i > $ultimaJornadaComunio - 5; $i--) {
            $sqlRacha = "SELECT points FROM players_historico where id=$id and jornada=$i";
            $resultadoRacha = $mysqli->query($sqlRacha);

            while ($rowRacha = $resultadoRacha->fetch_assoc()) {
                $totalPoints = $totalPoints + $rowRacha['points'];
            }
        }

        $racha = $totalPoints / 5;

		$query = "INSERT INTO players VALUES ($id, $idTeam, '$name','$valor','$points','$position','$partidosJugados','$status',$racha)";
		$mysqli->query($query);
	}
}