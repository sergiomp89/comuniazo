<?php
//lanzamos el script para actualizar  antes de empezar la jornada y de lanzar el algoritmo

include 'conexionDB.php';

//actualizamos equipos

$teams = $soap->getclubs();
$ultimaJornadaComunio = $soap->getlatestgameday();

foreach($teams as $equipo){
	$idTeam = $equipo->id;
	$players = $soap->getplayersbyclubidinclretired($idTeam);
	foreach($players as $jugador){
		$id = $jugador->id;
		$name = $jugador->name;
		$points = $jugador->points;
		$valor = $jugador->quote;
		$position = $jugador->position;
		$status = $jugador->status;

		//Coger partidos jugados
		$partidosJugados = '';
		$playerPartidosJugados = $soap->getplayersbyclubid($idTeam);
		foreach($playerPartidosJugados as $jugador2){
			if($jugador2->id == $id){
				$partidosJugados = $jugador2->rankedgamesnumber;
			}
		}

        $totalPoints = 0;
        $racha = 0;
        for ($i = $ultimaJornadaComunio; $i > $ultimaJornadaComunio - 5; $i--) {
            $sqlRacha = "SELECT points FROM players_historico where id=$id and jornada=$i";
            $resultadoRacha = $mysqli->query($sqlRacha);

            while ($rowRacha = $resultadoRacha->fetch_assoc()) {
                $totalPoints = $totalPoints + $rowRacha['points'];
            }
        }

        $racha = $totalPoints / 5;

		if(isset($partidosJugados)){
		
			$query = "UPDATE players SET points='$points',value='$valor',position='$position', partidos_jugados='$partidosJugados',status='$status', racha='$racha' where id=$id";
			$mysqli->query($query);
		}

	}
}